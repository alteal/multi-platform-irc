using System;
using Gtk;

public partial class MainWindow: Gtk.Window
{	
	MonoIRC.IRCClient client = null;

	public MainWindow (MonoIRC.IRCClient client): base (Gtk.WindowType.Toplevel)
	{
		this.client = client;
		Build ();
	}

	public void UpdateText (string text)
	{
		text += "\n";
		textview1.Buffer.Text += text;
	}

	public void ExitClean ()
	{
		if (client.sr != null)
			client.sr.Close ();
		if (client.sw != null)
			client.sw.Close ();
		if (client.ns != null)
			client.ns.Close ();
		if (client.IRCConnection != null)
			client.IRCConnection.Close ();
		if (client.connected == true)
			client.NetworkThread.Abort();
		Application.Quit ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		ExitClean();
		a.RetVal = true;
	}

	public void Scroll2 (object sender, Gtk.SizeAllocatedArgs e)
	{
		textview1.ScrollToIter (textview1.Buffer.EndIter, 0, false, 0, 0);
	}

	public void UpdateUserList (string username)
	{
		textview2.Buffer.Text += username + "\n";
	}

	protected void ServerConnect (object sender, EventArgs e)
	{
		//throw new System.NotImplementedException ();
		textview1.Buffer.Text += "Attempting to connect\n";
		this.textview1.SizeAllocated += new SizeAllocatedHandler(Scroll2);
		client.ServerConnect();
	}

	protected void Exit (object sender, EventArgs e)
	{
		ExitClean();
	}

	protected void Send (object sender, EventArgs e)
	{
		//if (entry1.Text = "") {
		Console.WriteLine (entry1.Text);
		if (client.connected == true) {
			//char[] charSeparator = new char[] { ' ' };
			string[] splitLine = entry1.Text.Split();
			//foreach (string x in splitLine)
			//{
			//	Console.WriteLine(x);
			//}
			if (splitLine[0] == "/join"){
				client.sendData ("JOIN", splitLine[1]);
			}
			else if (splitLine[0] == "/nickserv") {
				//Console.WriteLine(splitLine[2]);
				client.sendData ("NICKSERV", splitLine[1] + " " + splitLine[2]);
				//Console.WriteLine("nickserv " + splitLine[1] + " servvs " + splitLine[2]);
			}
			else
			{
				//Console.WriteLine(splitLine[0]);
				client.sendData ("PRIVMSG", "#MonoIRCTest" + " :" + entry1.Text);
			}
		}
		entry1.Text = "";
		//}
	}

}
