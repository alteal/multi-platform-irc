using System;
using Gtk;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace MonoIRC
{
	public struct IRCConfig
	{
		public string server;
		public int port;
		public string nick;
		public string name;
	}

	public class IRCClient
	{
		public TcpClient IRCConnection = null;
		IRCConfig config;
		public NetworkStream ns = null;
		public StreamReader sr = null;
		public StreamWriter sw = null;
		MainWindow win = null;
		public Boolean connected = false;
		public Boolean running = true;
		public Thread NetworkThread = null;

		public IRCClient (IRCConfig config)
		{
			this.config = config;
			Application.Init ();
			//Creates the window
			this.win = new MainWindow (this);
			this.win.SetPosition(WindowPosition.CenterAlways);
			//Application.Run ();
			GTKRun ();

		}

		public void ServerConnect(string username, string email, string name, string server, string port)
		{
			try
			{
				IRCConnection = new TcpClient(config.server, config.port);
				//win.UpdateText(config.server + config.port);
			} catch {
				win.UpdateText("Connection Error");
			}
			try 
			{
				ns = IRCConnection.GetStream();
				sr = new StreamReader(ns);
				sw = new StreamWriter(ns);
				sendData("USER", config.nick + " kostile.com " + " kostile.com" + " :" + config.name);
				sendData("NICK", config.nick);
				connected = true;
				NetworkThread = new Thread(new ThreadStart(IRCWork));
				NetworkThread.Start();
				//IRCWork ();
			} catch {
				win.UpdateText("Communication Error!");
			}
		}

		public void sendData(string cmd, string param)
		{
			if (param == null) {
				sw.WriteLine (cmd);
				sw.Flush ();
				//win.UpdateText (cmd);
			} else {
				sw.WriteLine (cmd + " " + param);
				sw.Flush ();
				//win.UpdateText (cmd + " " + param);
			}
		}

		public void GTKRun ()
		{
			Application.Run();
		}

		public string delegateShit (string data)
		{

			ManualResetEvent ev = new ManualResetEvent(false);
			Gtk.Application.Invoke(delegate {
				string reconstructed = "";
				String[] splitData = data.Split();
				if (data[0] == ':') {
					if (splitData[1] == "353"){
						for (int i = 5; i < splitData.Length - 5; i += 1)
						{
							win.UpdateUserList(splitData[i]);
						}
					} else if (splitData[1][0] == '3' || splitData[1][0] ==  '2' || splitData[1][0] == '0') {
						for (int i = 3; i < splitData.Length; i += 1)
						{
							reconstructed += " " + splitData[i];
						}
						win.UpdateText(reconstructed);
					} else {
						win.UpdateText(data);
					}
				} 
				ev.Set();
			});
			ev.WaitOne();
			return data;
		}

		public void IRCWork ()
		{
			string[] ex;
			string data;

			while (connected)
			{
				data = sr.ReadLine ();
				delegateShit(data);
				//Console.WriteLine (data);
				char[] charSeparator = new char[] { ' ' };
				ex = data.Split (charSeparator, 5);

				if (ex[0] == "PING")
				{
					sendData("PONG", ex[1]);
				}	
			}
		}
	}
}
