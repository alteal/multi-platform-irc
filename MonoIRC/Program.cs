using System;
using Gtk;
using System.Runtime.InteropServices;

namespace MonoIRC
{
	class MainClass
	{
		public static Boolean isConnected = false;
		public static Boolean isRunning = false;
		public static void Main (string[] args)
		{
			IRCConfig conf = new IRCConfig();
			conf.name = "servvs";
			conf.nick = "servvs";
			conf.port = 6667;
			conf.server = "irc.freenode.net";
            //Console.WriteLine(Convert.ToString((Environment.OSVersion.Platform.ToString())));
            if (Convert.ToString((Environment.OSVersion.Platform.ToString())) != "Win32NT")
                XInitThreads();
			new IRCClient(conf);
		}

	    [DllImport("libX11.so.6")]
    	extern public static int XInitThreads();
	}
}